import {fetch} from '../common/fetch'

export function addOrderList(params) {
    return fetch({
        url:'/settlement/addOrderList',
        method: 'get',
        params
    })
}

export function getOrderList(params) {
    return fetch({
        url:'/settlement/getOrderList',
        method: 'get',
        params
    })
}



