import {fetch} from '../common/fetch'

// 设置地址
export function setAddressById(params) {
    return fetch({
        url:'/address/setAddressById',
        method: 'get',
        params
    })
}
// 获取地址
export function getAddressById(params) {
    return fetch({
        url:'/address/getAddressById',
        method: 'get',
        params
    })
}