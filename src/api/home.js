import {fetch} from '../common/fetch'

// 获取顶部tab数据
export function getLytab() {
    return fetch({
        url:'/home/getLytab',
        method: 'get'
    })
}
// 获取推荐板块轮播图数据
export function getRecommendSwiper() {
    return fetch({
        url:'/home/recommend/swiper',
        method: 'get'
    })
}
// 获取国际板块轮播图数据
export function getInternationSwiper() {
    return fetch({
        url:'/home/internation/swiper',
        method: 'get'
    })
}
// 获取推荐板块分类数据
export function getRecommendCategory() {
    return fetch({
        url:'/home/recommend/category',
        method: 'get'
    })
}

// 获取推荐板块vip大牌日数据
export function getRecommendVip() {
    return fetch({
        url:'/home/recommend/vip',
        method: 'get'
    })
}

// 获取推荐板块今日特卖数据
export function getRecommendSpecialSales() {
    return fetch({
        url:'/home/recommend/specialSales',
        method: 'get'
    })
}
