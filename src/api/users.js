import {fetch} from '../common/fetch'

// 通过密码登录
export function loginByPassword(params) {
    return fetch({
        url:'/users/loginByPassword',
        method: 'get',
        params
    })
}

// 注册
export function registerByPassword(params) {
    return fetch({
        url:'/users/registerByPassword',
        method: 'get',
        params
    })
}

// 根据cookie中的用户id获取用户信息
export function getUserInfoById(params) {
    return fetch({
        url:'/users/getUserInfoById',
        method: 'get',
        params
    })
}