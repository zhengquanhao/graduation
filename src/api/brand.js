import {fetch} from '../common/fetch'

// 获取顶部tab数据
export function getBrandListById(params) {
    return fetch({
        url:'/brand/getBrandList',
        method: 'get',
        params
    })
}

// 获取顶部tab标题数据
export function getBrandTitle(params) {
    return fetch({
        url:'/brand/getBrandTitle',
        method: 'get',
        params
    })
}

