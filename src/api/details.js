import {fetch} from '../common/fetch'

// 获取详情数据
export function getBrandListById(params) {
    return fetch({
        url:'/details/getDetailsById',
        method: 'get',
        params
    })
}