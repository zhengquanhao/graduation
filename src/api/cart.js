import {fetch} from '../common/fetch'

// 添加商品到购物车(接口功能已包含1.添加商品到指定用户下2.之前已经添加过的商品只会增加数量,不插入新的一行)
export function addCartList(params) {
    return fetch({
        url:'/cart/addCartList',
        method: 'get',
        params
    })
}

// 根据用户账户获取购物车商品信息
export function getCartList(params) {
    return fetch({
        url:'/cart/getCartList',
        method: 'get',
        params
    })
}

// 根据用户账户删除购物车商品信息
export function deleteCartList(params) {
    return fetch({
        url:'/cart/deleteCartList',
        method: 'get',
        params
    })
}



