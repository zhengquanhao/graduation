//导入模块
import * as api_home from './home'
import * as api_brand from './brand'
import * as api_details from './details'
import * as api_users from './users'
import * as api_cart from './cart'
import * as api_address from './address'
import * as api_settlement from './settlement'

const apiObj = {
   api_home,
   api_brand,
   api_details,
   api_users,
   api_cart,
   api_address,
   api_settlement
}

const install = function (Vue) {
  if (install.installed) return
  install.installed = true

  //定义属性到Vue原型中
  Object.defineProperties(Vue.prototype, {
    $fetch: {
      get() {
        return apiObj
      }
    }
  })
}

export default {
  install
}
