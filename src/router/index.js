import Vue from 'vue'
import Router from 'vue-router'

const Home = () => import("../views/home/Home")
const Category = () => import("../views/category/Category")
const Cart = () => import("../views/cart/Cart")
const Profile = () => import("../views/profile/Profile")
const Brand = () => import("../views/brand/Brand")
const Details = () => import("../views/details/Details")
const Login = () => import("../views/login/Login")
const Register = () => import("../views/register/Register")
const Address = () => import("../views/address/Address")
const Order = () => import("../views/order/Order")

// 首页二级路由
const Recommend = () => import("../views/home/childComponents/Recommend")
const SnapUp = () => import("../views/home/childComponents/SnapUp")
const Internation = () => import("../views/home/childComponents/Internation")
const Luxury = () => import("../views/home/childComponents/Luxury")
const Advance = () => import("../views/home/childComponents/Advance")
const BeautyMakeup = () => import("../views/home/childComponents/BeautyMakeup")
const Kids = () => import("../views/home/childComponents/Kids")
const Life = () => import("../views/home/childComponents/Life")


Vue.use(Router)

export default new Router({
  routes: [
    {path:'/',redirect:'/home'},
    {
      path:'/home',
      component:Home,
      children: [
        {path:'/home',redirect:'/home/recommend'},
        {path:'recommend',component:Recommend},
        {path:'snapup',component:SnapUp},
        {path:'internation',component:Internation},
        {path:'luxury',component:Luxury},
        {path:'advance',component:Advance},
        {path:'beautyMakeup',component:BeautyMakeup},
        {path:'kids',component:Kids},
        {path:'life',component:Life}        
      ]
    },
    {path:'/cart',component:Cart},
    {path:'/profile',component:Profile},
    {path:'/category',component:Category},
    {path:'/brand/:brand_id',component:Brand},
    {path:'/productDetails/:productId',component:Details},
    {path:'/login',component:Login},
    {path:'/register',component:Register},
    {path:'/address',component:Address},
    {path:'/order',component:Order}
  ],
  mode:'history'
})