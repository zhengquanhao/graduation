import Vue from 'vue'
import App from './App.vue'
import api from './api'
import router from './router/index'

// 第三放
import Lytab from "ly-tab" // 横向滚动tab栏
import Vant from 'vant';
import 'vant/lib/index.css';

// import 'normalize.css/normalize.css'
Vue.use(api);
Vue.use(Lytab);
Vue.use(Vant);


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  api,
  router,
  Lytab
}).$mount('#app')
