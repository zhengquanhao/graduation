module.exports = {
  // 玛德代理不好使，后端做了Access-Control-Allow-Origin果真羞耻！
  // devServer: {
	// 	proxyTable : {
  //     "/api" :{
  //         target: "//localhost:3000/api/",
  //         ws: true,
  //         secure: false,
  //         changeOrigin: true,
  //         pathRewrite: {
  //             "^/api":""
  //         }
  //     }
  //   }
	// },
  configureWebpack: {
    resolve: {
      // 配置别名
      alias :{
        'assets': 'src/assets',
        'components': 'src/components',
        'views': 'src/views'
      }
    }
  },
  css: {
		// 启用 CSS modules
		modules: false,
		// 是否使用css分离插件
		extract: true,
		// 开启 CSS source maps，一般不建议开启
		sourceMap: false,
		// css预设器配置项
		loaderOptions: {
			sass: {
				//设置css中引用文件的路径，引入通用使用的scss文件（如包含的@mixin）
				data: `@/assets/variable.scss";`
			}
		}
	},
  assetsPublicPath:"/",

}


//npm  install  path  --save
const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir);
}


module.exports = {
  chainWebpack: config => {
    config.resolve.alias
      .set("@", resolve("src"))
      .set("assets", resolve("src/assets"))
      .set("components", resolve("src/components"))
      .set("views", resolve("src/views"))
  },
}
